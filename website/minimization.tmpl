{% extends "templates/base.tmpl" %}
{% block title %}Minimization Benchmark{% endblock %}
{% block content %}
<h2>Minimization Benchmarks</h2>

<p>
Entries for these benchmarks must record the average number number of force
calls as <i>force_calls</i> in <tt>benchmark.dat</tt>, the maximum number of
force calls as <i>force_calls_max</i>, the minimum number of force calls as
<i>force_calls_min</i>, and the number of failed minimizations as
<i>nfailed</i>
</p>

<h3><a id="lj38" href="#lj38">Lennard-Jones 38 Clusters</a></h3>

<p>This benchmark tests the performance of optimizers starting from 1000
randomly generated Lennard-Jones 38 clusters. A tar file containing the
structures is here: <a href="minimization/lj38/lj38-clusters.tgz">lj38.tgz</a>.</p>


<p>The L2 norm of the force must be reduced to at least 0.01 energy /
distance. The maximum number of force calls that may be made cannot exceed
10,000. Runs that exceed the maximum number of force calls or fail to converge for 
any other reason are considered failed.</p>

<table class="sortable table table-striped table-condensed table-bordered table-hover benchmark-table">
  <thead>
    <th>Entry</th>
    <th>&lt;N&gt;</th>
    <th>min N</th>
    <th>max N</th>
    <th>Failed</th>
  </thead>
  {% for entry in benchmarks['lj38']|sort(attribute='force_calls') %}
    {% if entry.hidden == False %}
    <tr>
    {% else %}
    <tr class="hidden-lj38" style="display: none;">
    {% endif %}
      <td>{% include 'templates/entry-details.tmpl' %}</td>
      <td>{{ '%.0f' % entry.force_calls }}</td>
      <td>{{ '%.0f' % entry.force_calls_min }}</td>
      <td>{{ '%.0f' % entry.force_calls_max }}</td>
      <td>{{ '%.0f' % entry.nfailed }}</td>
    </tr>
  {% endfor %}

  <tr>
    <td colspan="5">
      <a href="#" onclick="$('.hidden-lj38').toggle();return false;">Show/Hide Additional Entries...</a>
    </td>
  </tr>

</table>

<h3><a id="morse-bulk" href="#morse-bulk">Morse Bulk</a></h3>

<p>This benchmark tests the performance of optimizers starting from 100 FCC
bulk Morse structures that have been slightly perturbed from their equilibrium
lattice positions. A tar file containing the structures is here: 
<a href="minimization/morse-bulk/morse-bulk.tgz">morse-bulk.tgz</a>.</p>

$$ U(r) = D_e ( 1 - e^{-a(r-r_e)})^2 $$
<p>where $D_e=0.7102$ eV, $r_e=2.8970$ Ang, and $a=1.6047$ Ang$^{-1}$.</p>


<p>The norm of the force must be reduced to at least 1e-3 eV/Ang</p>

<table class="sortable table table-striped table-condensed table-bordered table-hover benchmark-table">
  <thead>
    <th>Entry</th>
    <th>&lt;N&gt;</th>
    <th>min N</th>
    <th>max N</th>
  </thead>
  {% set any_hidden = False %}
  {% for entry in benchmarks['morse-bulk']|sort(attribute='force_calls') %}
    {% if entry.hidden == False %}
    <tr>
    {% else %}
    {% set any_hidden = True %}
    <tr class="hidden-morse" style="display: none;">
    {% endif %}
      <td>{% include 'templates/entry-details.tmpl' %}</td>
      <td>{{ '%.0f' % entry.force_calls }}</td>
      <td>{{ '%.0f' % entry.force_calls_min }}</td>
      <td>{{ '%.0f' % entry.force_calls_max }}</td>
    </tr>
  {% endfor %}
  {% if any_hidden %}
  <tr>
    <td colspan="5">
      <a href="#" onclick="$('.hidden-morse').toggle();return false;">Show/Hide Additional Entries...</a>
    </td>
  </tr>
  {% endif %}
</table>
{% endblock %}
